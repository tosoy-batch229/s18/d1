// [ SECTION ] - Parameters and Arguments
// functions in JS are lines or blocks of codes that tells our devices or applications to perform a specific task.
// Last session we learned how to use a basic function.

function printInput() {
    let nickname = prompt("Enter your nickname: ");
    console.log(`Hi, ${nickname}!`);
}
// printInput();

// In some cases, a basic function may not be ideal.
// For other cases, function can also process data directly into it instead.

// Consider this function
// Parameter is located inside the parentheses after the function name.
function printName(name) {
    console.log(`My name is ${name}`);
}
// Arguments is located in the invocation of the function. The data will be passed on the parameter which can be used inside the code blocks of a particular function.
printName("Juana");
printName("John");
printName("Jane");

// Variables can be passed as an argument.
let sampleVariable = "Yui"
printName(sampleVariable);

// Function arguments cannot be used by functions if there are no parameters provided.
function checkDivisibilityBy8(number) {
    let remainder = number % 8;
    console.log(`The remainder of ${number} divided by 8 is: ${remainder}`);
    let isDivisibleBy8 = remainder === 0;
    console.log(`Is ${number} divisible by 8?`);
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Function as arguments.
function argumentFunction() {
    console.log(`This function was passed as an argument before the message was printed.`);
}

function invokeFunction(argumentFunction) {
    argumentFunction();
}
invokeFunction(argumentFunction);

// Finding more info about a function we can use console.log(functionName);
console.log(argumentFunction);

// Function - Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters".

function createFullName(firstName, middleName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
createFullName("Juan", "Dela", "Cruz");

// "Juan" will be stored in the parameter "firstname"
// "Dela" will be stored in the parameter "middlename"
// "Cruz" will be stored in the parameter "lastname"

// The "lastname" parameter will returned undefined value.
createFullName("Juan", "Dela");
// The "Hello" will be disregarded since the function is expecting 3 arguments only.
createFullName("Juan", "Dela", "Cruz", "Hello");

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";
createFullName(firstName, middleName, lastName);

// Parameter names are just names refer to the arguments.
// The order of the arguments must be the same order with the parameters.
function printFullName(middleName, firstName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("Juan", "Dela", "Cruz");

// The "return" statement
function returnFullName(firstName, middleName, lastName) {
    console.log("Test console message.");
    return `${firstName} ${middleName} ${lastName}`;
    console.log("This message will not be printed.");
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

// returnFullName("Jeffrey", "Smith", "Bezos") -> This will not work if our function is expecting a return value, returned values should be stored in a variable.

console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain the result and returned its value.
function returnAddress(city, country) {
    let fullAddress = city + ", " + country;
    return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// On the other hand, when a function only has console.log() to display, its result will return "undefined" instead.
function printPlayerInfo(userName, level, job) {
    console.log("Username: " + userName);
    console.log("Level: " + level);
    console.log("Job: " + job);
}

let user1 = printPlayerInfo("Knight_White", 95, "Paladin");
console.log(user1);

printPlayerInfo("Knight_White", 95, "Paladin");

// Returns undefined because printPlayerInfo returns nothing.